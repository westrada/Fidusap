This API includes any service related to natural and legal clients.
This API includes any service related to the management of the fund and risk profile.
Investment funds are assets belonging to a number of private investors and other Collective Investment Institutions, called unitholders, which invest in all types of financial assets (stocks, bonds, currencies, etc.) or non-financial assets (real estate, Art, etc.).
The risk profile indicates the investor's ability to take losses, always taking into account that the greater the risk is willing to assume, the greater the return obtained from the investment.
